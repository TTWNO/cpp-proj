/* +--------------------------------------------------------+
 * | Serial port interface program                          |
 * | Written by: Justin N. Pilon                            |
 * | Communicates to Raspberry Pi                           |
 * | Compile using: g++ -std=c+11 -pthread -o main main.cpp |
 * +--------------------------------------------------------+
 */

#include <string>    //
#include <iostream>  //
#include <stdio.h>   // standard input / output functions
#include <string.h>  // string function definitions
#include <unistd.h>  // UNIX standard function definitions
#include <fcntl.h>   // File control definitions
#include <errno.h>   // Error number definitions
#include <termios.h> // POSIX terminal control definitionss
#include <time.h>    // time calls

int open_port(void) // open port
{
  int fd; // fd = serial port (F)ile (D)escription
  fd = open("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);
  if(fd == -1) // fd opened successfully
  {
    printf("open_port: unable to open /dev/ttyUSB0\n\n");
  }
  else
  {
    fcntl(fd, F_SETFL, 0);
    printf("port is open\n\n");
  }

  return(fd);
}

int configure_port(int fd) // configure port
{
  struct termios port_settings; // structure to store the port settings in
  cfsetispeed(&port_settings, B115200); // set baud rates to 115200
  cfsetospeed(&port_settings, B115200);
  port_settings.c_cflag &= ~PARENB; // set no parity, stop bits, data bits
  port_settings.c_cflag &= ~CSTOPB;
  port_settings.c_cflag &= ~CSIZE;
  port_settings.c_cflag |= CS8;
  tcsetattr(fd, TCSANOW, &port_settings); // apply the settings to the port
  return(fd);
}

int query_modem(int fd,int timeInput) // query modem with an AT command
{
  char n;
  fd_set rdfs;
  struct timeval timeout;

  // timeout structure
  timeout.tv_sec = timeInput;
  timeout.tv_usec = 0;
  int total = timeout.tv_sec;

  // create string message and byte array
  unsigned char str[] = { "your message" };
  unsigned char send_bytes[] = { 0x02, 0x00, 0x01, 0x23, 0x01, 0x21, 0x03};

  write(fd, str, sizeof(str)); // send string
  write(fd, send_bytes, sizeof(send_bytes)); // send data in bytes
  printf("the following messages were sent: %s, %s\n", str, send_bytes);

  // do the select
  n = select(fd + 1, &rdfs, NULL, NULL, &timeout);

  // check if an error has occured
  if(n < 0)
  {
    perror("select failed\n");
  }
  else if (n == 0)
  {
    //puts("Timeout!\n");
    std::cout << "time: " << timeout.tv_sec << "/" << total << " sec\n" << std::endl;
  }
  else
  {
    printf("bytes detected on the port\n");
  }

  return 0;

}

int main(void) // main
{
  int fd = open_port();
  configure_port(fd);

  int i;
  for (i = 1; i < 5; ++i)
  {
    query_modem(fd, 3);
  }

  return(0);
}
