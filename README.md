# Random C++ project
# This is a random comment!
# Tait: I am changing the main branch so that I can
#       see a nice arc in the branch out and merge.

This is a project me and Tait are working on.

### Uses:
This will mostly be for testing git techniques.

#### Compile:

To compile this project, simply use the following:

```bash
make
```

To remove all files from the build process, use the following:

```bash
make clean
```
